# NWON-Shell-Environment

This is a tool for facilitating the installation and configuration of [oh-my-zsh](https://github.com/ohmyzsh/ohmyzsh). More importantly it make it easier to share shell setups.

## Installation

Run the `scripts/nwon-se-install.sh` and you are hopefully all set :).

Maybe you want to adjust the selected user and/or the selected environment afterwards. For doing so just adjust the `.env` in the main folder or use the scripts `scripts/nwon-se-set-environment.sh` and `scripts/nwon-se-set-user.sh`.

## The shell environment

### Structure

In general a shell environment has a user set in the `env` file via the `$NWON_SE_USER` variable.

We provide a base-user which works as a starting point for every other environment. It is stored in: `users/base-user`.

Individual settings are applied using the following order:

- Base user (`users/base-user`)
- Custom user (`users/${NWON_SE_USER}`)

### Add your own user

If you want to set up your own user it is best to copy `users/00-EXAMPLE-USER` and rename it according to your wishes. This will provide you with the appropriate folder structure.

### Folder structure user

A user has 5 folders

- dotfiles-linked-to-home: Files that are linked to `~`  during installation
- files-to-source:
  - zshenv: Files that are sourced in zshenv
  - zshrc: Files that are sourced in zshrc
  - bashrc: Files that are sourced in zshrc
- static:
  - files: Files that are required in this environment
  - apt: Backup of apt sources and packages
- scripts: Custom scripts of the user that gets exported to `$PATH`.
- static: Files that should be shared between different environments. Usually these are config files like the VSCode configuration.

Furthermore there are two files: constants.sh and pre-shell-install-entrypoint.sh:

- constants.sh: Shell variables that are used during setup. For example a list of zsh plugins or a list of default apt packages. 
- pre-shell-install-entrypoint.sh: A file that is sourced after installation of apt packages and zsh but before all config files are linked. Here is the point to install custom zsh extensions.
  
## Backup

Before linking a file or copying something from the NWON-Shell-Environment files are backed up in `backup`.

## Debug mode

If you set `NWON_SE_DEBUG_MODE` to `1` additional output is logged.

## Testing

A docker setup is provided for setting up an Ubuntu and installing the environment configured in the `docker/.env` file. Afterwards you can connect to the container and check the result.
