# export ZSH_START_MILLISECONDS=$(($(date +%s%N) / 1000000))

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
    export EDITOR='vi'
else
    export EDITOR='vi'
fi

export LOCAL_EDITOR=code

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# source config file that exports the path to nwon-shell-environment
source "${HOME}/.nwon-shell-environment"

# source linux setup utilities
source ${NWON_SE_BASE_DIR}/scripts/nwon-se-utils.sh

# source files modifying zshenv
sourceSourceFiles "files-to-source/zshenv"

# source default bash aliases
if [ -f ~/.bash_aliases ]; then
    source ~/.bash_aliases
fi
