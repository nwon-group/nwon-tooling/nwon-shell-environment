# Poetry - Python Package Manager

# auto enter virtual environments
if which pyenv-virtualenv-init >/dev/null; then
    eval "$(pyenv virtualenv-init -)"
fi

if command -v pyenv 1>/dev/null 2>&1; then
    eval "$(pyenv init -)"
fi
