#!/bin/bash
set -euo pipefail

snap install bitwarden
snap install mailspring
snap install postman
snap install skype
snap install slack
snap install teams-for-linux
