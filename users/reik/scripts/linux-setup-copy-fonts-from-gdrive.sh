#!/bin/bash
set -euo pipefail

cp -rv $GDRIVE/fonts/* /usr/local/share/fonts
