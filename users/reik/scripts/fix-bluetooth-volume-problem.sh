#!/bin/bash

source $(dirname "${BASH_SOURCE[0]}")/../../../scripts/nwon-se-utils.sh

userHasSudoRights

NOW=$(date +"%Y-%m-%d_%I-%M-%S")

# Install dependencies:
sudo apt install pulseaudio pulseaudio-utils pavucontrol pulseaudio-module-bluetooth

# add configuration file:

if [ -f /etc/bluetooth/audio.conf ]; then
    cp -v /etc/bluetooth/audio.conf backup/${NOW}_audio.conf 2>/dev/null
fi

sudo touch /etc/bluetooth/audio.conf

sudo echo "# This section contains general options
[General]
Enable=Source,Sink,Media,Socket" | sudo tee -a /etc/bluetooth/audio.conf >/dev/null

# restart your bluetooth
sudo service bluetooth restart
