#!/bin/bash
set -euo pipefail

source $(dirname "${BASH_SOURCE[0]}")/../../../scripts/nwon-se-utils.sh

userHasSudoRights

sudo fc-cache -r -v
