#!/bin/bash
set -euo pipefail

setting_files=("albert.conf")
albert_config_path=~/.config/albert

cd $albert_config_path

for setting_file in "${setting_files[@]}"; do
    :
    printf "\n Process $setting_file:\n"
    rm $albert_config_path/$setting_file
    ln -s ${NWON_SE_CONFIGS_DIR}/$setting_file $setting_file
done
