#!/bin/bash
set -euo pipefail

# install something before something shell related is installed

# install powerlevel 10k theme
rm -rf ~/.oh-my-zsh/custom/themes/powerlevel10k &>/dev/null
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/themes/powerlevel10k
