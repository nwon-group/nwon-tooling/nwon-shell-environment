#!/bin/bash
set -euo pipefail

# source utils
source $(dirname $(dirname "${BASH_SOURCE[0]}"))/nwon-se-utils.sh

${NWON_SE_SCRIPTS_DIR}/backup/nwon-se-backup-apt-packages.sh
${NWON_SE_SCRIPTS_DIR}/backup/nwon-se-backup-apt-sources.sh

${NWON_SE_SCRIPTS_DIR}/nwon-se-git-push.sh -m "Update installed packages and sources"
