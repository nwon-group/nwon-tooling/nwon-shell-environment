#!/bin/bash
set -euo pipefail

# source utils
source $(dirname $(dirname "${BASH_SOURCE[0]}"))/nwon-se-utils.sh

userHasSudoRights

SOURCES_FILE=${NWON_SE_USER_ENVIRONMENT_STATIC_DIR}/apt/sources.list

mv $SOURCES_FILE /etc/apt/sources.list
sudo apt-get update
