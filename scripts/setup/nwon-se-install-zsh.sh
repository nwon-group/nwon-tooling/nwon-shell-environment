#!/bin/bash
set -euo pipefail

# source utils
source $(dirname $(dirname "${BASH_SOURCE[0]}"))/nwon-se-utils.sh

install_zsh() {
  # install system dependencies
  if [ ${NWON_SE_USER_HAS_SUDO_RIGHTS:?} == "1" ]; then
    sudo apt-get --assume-yes install \
      zsh \
      zsh-common \
      powerline \
      fonts-powerline # 🚨🚨 The fonts will only get installed when zsh is not installed already.

    # make zsh the default shell
    sudo usermod -s /usr/bin/zsh $(whoami)
  else
    logError "No sudo access. Cannot install zsh properly."
  fi

}

# install oh-my-zsh to default directory ~/.oh-my-zsh
install_oh_my_zsh() {
  if [ -d "${HOME:?}/.oh-my-zsh" ]; then
    logInfo "${HOME:?}/.oh-my-zsh already exist"
  else
    wget https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh && sh install.sh --unattended || {
      echo "Could not install Oh My Zsh" >/dev/stderr
      exit 1
    }
  fi
}

# only install zsh if not installed already.
which zsh || install_zsh

install_oh_my_zsh
