#!/bin/bash
set -euo pipefail

exportNwonSeBaseDir() {
    # Absolute path to this script, e.g. /home/user/bin/foo.sh
    SCRIPT=$(readlink -f "$0")

    # Absolute path this script is in, thus /home/user/bin
    SCRIPTPATH=$(dirname "$SCRIPT")

    echo $(dirname $SCRIPTPATH)

    export NWON_SE_BASE_DIR=$(dirname $SCRIPTPATH)
}

# store directory from NWON-shell-environment in config file
createConfigFile() {
    export NWON_SE_CONFIG_FILE=~/.nwon-shell-environment

    #create or clean config file
    touch ${NWON_SE_CONFIG_FILE}
    >${NWON_SE_CONFIG_FILE}

    echo "export NWON_SE_BASE_DIR=${NWON_SE_BASE_DIR}" >>${NWON_SE_CONFIG_FILE}
}

exportNwonSeBaseDir
createConfigFile
