#!/bin/bash
set -euo pipefail

# source utils
source $(dirname $(dirname "${BASH_SOURCE[0]}"))/nwon-se-utils.sh

userHasSudoRights

sudo apt-get update && sudo apt-get -y install ${NWON_SE_APT_PACKAGES}
