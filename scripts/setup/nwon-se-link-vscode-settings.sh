#!/bin/bash
set -euo pipefail

# source utils
source $(dirname $(dirname "${BASH_SOURCE[0]}"))/nwon-se-utils.sh

setting_files=("settings.json" "syncLocalSettings.json" "snippets" "keybindings.json")

if isMacOs; then
    vscode_config_path=~/Library/Application\ Support/Code/User
else
    vscode_config_path=$HOME/.config/Code/User
fi

vscode_config_path=$HOME'/Library/asd/Code/User'

echo $vscode_config_path

for setting_file in "${setting_files[@]}"; do
    :
    backupFile ${vscode_config_path}/$setting_file
    linkFile ${NWON_SE_USER_STATIC_DIR}/VSCode/$setting_file ${vscode_config_path}/$setting_file
done
