#!/bin/bash
set -euo pipefail

usage="$(basename "$0") [-h] [-c]

script for pushing current changes in nwon-shell-environment

where:
    -h  show this help text
    -m  set the desired commit message"

# handle arguments
while getopts ":hm:" opt; do
  case $opt in
  h)
    echo "$usage"
    exit
    ;;
  m) COMMIT_MESSAGE="$OPTARG" ;;
  \?)
    echo "Invalid option -$OPTARG" >&2
    echo "$usage"
    exit
    ;;
  esac
done
shift $((OPTIND - 1))

NOW=$(date +"%Y-%m-%d_%I-%M-%S")

cd ${NWON_SE_BASE_DIR}
git stage -A
git commit -m "${NOW} ${COMMIT_MESSAGE}"
git push
